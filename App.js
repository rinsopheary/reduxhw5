import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './components/splashscreen/SplashScreen'
import LoginScreen from './components/loginScreen/LoginScreen'
import HomeScreen from './components/homescreen.js/HomeScreen';
import DetailScreen from './components/detailScreen/DetailScreen'
import PostArticleScreen from './components/PostArticleScreen';
import CategoryScreen from './components/CategoryScreen';

//redux import
import { Provider } from 'react-redux'
import { centralStore } from './redux/stores/centralStore'

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={centralStore}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Splash" 
        component={SplashScreen} 
        options = {{headerShown:null}}
        />

          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: null }}
          />

          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: null }}
          />

          <Stack.Screen
            name="Detail Screen"
            component={DetailScreen}
          options = {{title : 'លម្អិត'}} 
          />

          <Stack.Screen
            name="Add Screen"
            component={PostArticleScreen}
          // options = {{headerShown:null}} 
          />

          <Stack.Screen
            name="Category Screen"
            component={CategoryScreen}
          // options = {{headerShown:null}} 
          />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;

export const baseURL = 'http://110.74.194.124:15011/'

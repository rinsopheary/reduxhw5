import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList, ClippingRectangle } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import { fetchCategory, setCategory } from '../redux/actions/articleAction'


export class CategoryScreen extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.fetchCategory()
    }
    render() {
        const { navigation } = this.props
        return (
            <View>
                <FlatList
                    data={this.props.categories}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => {
                            console.log(item.id)
                            console.log('itiitit')
                            this.props.setCategory(item.id)
                            navigation.goBack()
                        }}>
                            <Text> {item.name} </Text>
                        </TouchableOpacity>
                    )}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = (store) => {
    return {
        categories: store.articleR.categories
    }
}

export default connect(mapStateToProps, { fetchCategory, setCategory })(CategoryScreen)

import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, Image, TextInput, Button } from 'react-native'
import { postArticle } from '../redux/actions/articleAction'
import {connect} from 'react-redux'

const imageURL = require('./assets/images/logo.png')
export class PostArticleScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
        }
    }

    render() {
        const { navigation, route } = this.props
        return (
            <SafeAreaView>
                <Text> Add Article </Text>
                <Image
                    source={imageURL}
                    style={{ width: '100%', height: 100 }}
                    resizeMode="contain"
                />
                <Text>Title</Text>
                <TextInput

                />
                <Text>Description</Text>
                <TextInput />
                <Text>Category</Text>
                <View style={{ borderColor: 'black', borderWidth: 1 }}>
                    <Button
                        title=''
                        onPress={() =>
                            navigation.navigate('Category Screen')
                        }

                    />
                </View>

                <Button
                    title='Post'
                    onPress={() => {
                        this.props.postArticle('title dddddd', 'description dddd', 'https://cdn-images-1.medium.com/max/1024/1*onuKAwAHdvT3zHQkdJodlg.jpeg', this.props.userId, this.props.categoryId)
                        navigation.goBack()
                    }}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = (store) => {
    return {
        categoryId: store.articleR.categoryId,
        userId : store.userR.user.id
    }
}

export default connect(mapStateToProps, { postArticle })(PostArticleScreen)

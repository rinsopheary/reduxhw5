import {Body, Card, CardItem, H3, List, ListItem} from 'native-base';
import React, {Component} from 'react';
import {SafeAreaView, Text, View, Image} from 'react-native';
import {connect} from 'react-redux';
import {fetchOneArticle, removeArticle} from '../../redux/actions/articleAction'

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    console.log(this.props.route.params.id)
    this.props.fetchOneArticle(this.props.route.params.id);
    console.log('did mount in one article')
    // console.log(this.props.article)
    console.log('end did mount in one ')
    // console.log('---->'+ this.props.route.params.id);
  }

  componentWillUnmount () {
    this.props.removeArticle()
  }

  render() {
    return (
      <SafeAreaView>
        <Card>
          {this.props.article == null ? <Text>Not Found</Text> 
          : 
          <View>
            <Image 
              source={{uri : this.props.article.image_url}} 
              style={{width: '100%', height: 200, resizeMode : "contain"}}
            />
            <Text style={{fontSize: 15, marginTop : 20}}>{this.props.article.description}</Text>
          </View>
          }
        </Card>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (store) => {
    return {
        article: store.articleR.article
    }
}

export default connect(mapStateToProps, { fetchOneArticle, removeArticle })(DetailScreen)

import React, { Component } from 'react';
import { Container, Header, Input, Content, Item, Footer, FooterTab, Button, Text, Right, Card } from 'native-base';
import { StyleSheet, Image, View, FlatList, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
//redux import
import { connect } from 'react-redux'
import { fetchArticle } from '../../redux/actions/articleAction'

export class HomeScreen extends Component {

    componentDidMount() {
        console.log('HomeScreen work');
        this.props.fetchArticle();
        console.log('HomeScreen after fetch')
        console.log(this.props.articles)
        console.log('HomeScreen did mount')
    }

    getDate(date) {
        let y = date.substring(0, 4);
        let m = date.substring(4, 6);
        let d = date.substring(6, 8);
        return d + "-" + m + "-" + y;
    }

    render() {
        const {navigation} = this.props;

        console.log(this.props.articles);
        return (
            <Container>
                <Header
                    searchBar rounded
                    style={{ backgroundColor: "#bce6eb" }}
                >
                    <Item style={{ backgroundColor: 'white' }}>
                        <Icon
                            name="search"
                            style={styles.searchIconStyle}
                        />
                        <Input placeholder="Search" />
                        <Icon
                            name="book"
                            style={styles.searchIconStyle}
                        />
                    </Item>

                    <Right>
                        <Button  
                        transparent>
                         
                            <Icon
                                name="bars"
                                style={styles.menuIcon}
                            />
                        </Button>
                        <Button 
                        onPress={() =>
                                    navigation.navigate('Add Screen')
        
                                }
                        transparent>
                            <Icon
                                name="plus"
                                style={styles.searchIconStyle}
                            />
                        </Button>
                    </Right>
                </Header>

                <Content>
                    <Text style={styles.dailyText}>Your Daily Read</Text>
                    <FlatList
                        data={this.props.articles}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate('Detail Screen', { id: item.id })
        
                                }
                                >

                                <Card>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 15 }}>
                                        <View style={{ width: '70%' }}>
                                            <Text numberOfLines={2}
                                                style={{ fontSize: 20, fontWeight: "bold", marginBottom: 12 }}
                                            >{item.title}</Text>
                                            <Text style={{ fontSize: 15, marginBottom: 8 }}>{item.description}</Text>
                                            <Text style={{ color: 'grey' }}>{this.getDate(item.created_date)}</Text>
                                        </View>
                                        <View style={{ width: '30%' }}>
                                            {/* <Image source={item.image_url} style={{ width: 120, height: 120, resizeMode: "cover" }} /> */}
                                            <Image source={{uri:item.image_url}} style={{ width: 120, height: 120, resizeMode: "cover" }}>
                                            </Image>
                                        </View>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )}
                    />
                </Content>
                <Footer style={{ backgroundColor: "#bce6eb" }}>
                    <FooterTab>
                        <Button vertical>
                            <Icon name="home" style={styles.searchIconStyle} />
                            <Text>Home</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="user" style={styles.searchIconStyle} />
                            <Text>User</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    searchIconStyle: {
        fontSize: 20
    },
    menuIcon: {
        fontSize: 20,
        marginRight: 30
    },
    dailyText: {
        fontSize: 25,
        fontWeight: "bold",
        padding: 12,
        // backgroundColor : 'gray'
    }
})
const mapStateToProps = (store) => {
    return {
        articles: store.articleR.articles
    }
}

export default connect(mapStateToProps, { fetchArticle })(HomeScreen)

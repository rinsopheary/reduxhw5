import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native'
import { Button } from 'native-base'
import { postUser } from '../../redux/actions/userAction'
import { connect } from 'react-redux'


class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        };
    }

    componentDidUpdate() {
        if (this.props.user != null) {
            this.props.navigation.navigate("Home")
        } else {
        }
    }

    onLogin() {
        this.props.postUser(this.state.username, this.state.password)
        console.log(this.props.user);
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => Keyboard.dismiss()}
            >
                <View style={styles.container}>
                    <View style>
                        <Text style={styles.inputext}> HOLY </Text>
                    </View>
                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                        placeholder='Email...'
                        placeholderTextColor='white'
                        style={styles.input}
                        returnKeyType="go"
                        autoCapitalize={false}
                    />
                    <TextInput
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        placeholder='Password...'
                        placeholderTextColor='white'
                        secureTextEntry={true}
                        style={styles.input}
                        returnKeyType="done"
                        autoCapitalize={false}
                        onSubmitEditing={() => this.onLogin()}
                    />

                    <Button full style={styles.btnStyle} danger
                        onPress={() => this.onLogin()}
                    >
                        <Text style={{ color: 'white', fontWeight: "bold" }}>LOGIN</Text>
                    </Button>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 45
    },
    input: {
        // width: 200,
        // height: 44,
        width: '100%',
        padding: 15,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 20,
        backgroundColor: 'black',
        color: 'white'
    },
    inputext: {
        width: '100%',
        fontSize: 50,
        fontWeight: 'bold',
        color: '#f05454',
        textAlign: "center",
        alignContent: "center",
        marginBottom: 30
    },
    btnStyle: {
        marginTop: 30,
        borderRadius: 20,
    }
})

const mapStateToProps = (store) => {
    return {
        user: store.userR.user,
        isLogin: store.userR.isLogin
    }
}

export default connect(mapStateToProps, { postUser })(LoginScreen)
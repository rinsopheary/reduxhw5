export const actionType = {
    def : 'default',
    fetch_article : 'fetch_article',
    post_user : 'post_user',
    fetch_one_article : 'fetch_one_article',
    remove_article : 'remove_one_article',
    post_article : 'post_article',
    fetch_category : 'fetch_category',
    set_category : 'set_category',

}
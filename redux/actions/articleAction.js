import { baseURL } from './../../App'
import { actionType } from './actionType'

export const fetchArticle = () => {
    return (dispatch) => {
        fetch(`${baseURL}v1/api/articles`, {
            method: 'GET', headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                console.log(result)
                return dispatch({
                    type: actionType.fetch_article,
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }
}

export const fetchOneArticle = (id) => {
    return (dispatch) => {
        fetch(`${baseURL}v1/api/articles/${id}`, {
            method: 'GET', headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                // console.log(result)
                return dispatch({
                    type: actionType.fetch_one_article,
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }
}

export const fetchCategory = () => {
    return (dispatch) => {
        fetch(`${baseURL}v1/api/categories`, {
            method: 'GET', headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                // console.log(result)
                return dispatch({
                    type: actionType.fetch_category,
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }
}



export const removeArticle = () => {
    return (dispatch) => { return dispatch({ type: actionType.remove_article }) }
}


export const setCategory = (id) => {
    return (dispatch) => { console.log(id); return dispatch({ type: actionType.set_category, payload: id }) }
}

export const postArticle = (title, description, image, authorId, categoryId) => {
    // var title = "string"
    // var description = "string"
    // var image = "string"
    // var authorId = 1000
    // var categoryId = 0
    console.log(authorId, categoryId)
    return (dispatch) => {
        fetch(`${baseURL}v1/api/articles`, {
            method: 'POST', headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "title": title,
                "description": description,
                "image": image,
                "author_id": authorId,
                "category_id": categoryId
            })
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                console.log(result)
                return dispatch({
                    type: actionType.def,
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }

}
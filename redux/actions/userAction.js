import { baseURL } from './../../App'
import { actionType } from './actionType'


export const postUser = (email, password) => {
    // email = 'admin123@admin.com'
    // password = '123'
    return (dispatch) => {
        fetch(`${baseURL}v1/api/user/authentication`, {
            method: 'POST', headers: {
                Authorization: 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "email": email,
                "password": password
            })
        })
        .then(res => {
            return res.json()
        })
        .then(result => {
            console.log(result.data)
            return dispatch({
                type: actionType.post_user,
                payload: result.data
            })
        }
        )
        .catch(error => {
            console.log(error)
            return ({})
        })
    }
}
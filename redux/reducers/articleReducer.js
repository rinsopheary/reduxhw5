import { actionType } from '../actions/actionType'

const initState = {
    articles: [],
    isLogin: false,
    user: null,
    article: null,
    categories: [],
    category_id: null
}

export const articleReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.fetch_article:
            return { ...state, articles: action.payload }
        case actionType.fetch_one_article:
            return { ...state, article: action.payload }
        case actionType.remove_article:
            return { ...state, article: null }
        case actionType.fetch_category:
            return { ...state, categories: action.payload }
        case actionType.set_category:
            console.log('in payload')
            console.log(action.payload)
            console.log('in payload')
            return { ...state, category_id: action.payload }
        case actionType.def:
            return state
        default:
            return state
    }
}
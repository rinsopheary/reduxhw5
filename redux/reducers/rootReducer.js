import {combineReducers} from 'redux'
import {articleReducer} from './articleReducer'
import { userReducer } from './userReducer'

export const rootReducer = combineReducers({
    articleR : articleReducer,
    userR : userReducer
})
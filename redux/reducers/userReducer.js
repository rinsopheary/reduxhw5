import {actionType} from '../actions/actionType'

const initState  = {
    user: null
}

export const userReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.post_user:
            return {...state, user : action.payload}
        default:
            return state
    }
}